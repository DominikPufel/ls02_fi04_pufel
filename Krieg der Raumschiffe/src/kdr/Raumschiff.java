package kdr;

import java.util.ArrayList;
import java.util.Random;
/**
 * Das ist die Klasse Raumschiff, die alle dazugeh�rigen Methoden enth�lt.
 * @author Dominik Pufel
 * 
 */
public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Standartkonstruktor
	/**
	 * Standartkonstruktor der Klasse Raumschiff
	 */
	public Raumschiff() 
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	/**
	 * Der individuelle Konstruktor, durch den ein Raumschiff in der Main Methode erstellt wird und die Variablen festgelegt werden.
	 * @param photonentorpedoAnzahl enth�lt die Anzahl der Photonentorpedos, die das Raumschiff enth�lt
	 * @param energieversorgungInProzent beschreibt die Energieversorgung des Raumschiffs in Prozent als Integer
	 * @param zustandSchildeInProzent beschreibt die Schilde des Raumschiffs in Prozent als Integer
	 * @param zustandHuelleInProzent beschreibt die Huelle des Raumschiffs in Prozent als Integer
	 * @param zustandLebenserhaltungssystemeInProzent beschreibt die Lebenserhaltungssysteme des Raumschiffs in Prozent als Integer
	 * @param anzahlDroiden beschreibt die Anzahl der Droiden die das Raumschiff an Board hat als Integer
	 * @param schiffsname legt den Namen f�r das erstellte Raumschiff fest
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent, 
					  int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden, String schiffsname)
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = schiffsname;
	}
	
	/**
	 * In diesem Bereich werden die Getter und Setter festgelegt
	 * 
	 */
	public int getPhotonentorpedoAnzahl() 
	{
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) 
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() 
	{
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int zustandenergieversorgungInProzentNeu) 
	{
		this.energieversorgungInProzent = zustandenergieversorgungInProzentNeu;
	}

	public int getSchildeInProzent() 
	{
		return schildeInProzent;
	}

	public void setSchildeInProzent(int zustandschildeInProzentNeu) 
	{
		this.schildeInProzent = zustandschildeInProzentNeu;
	}

	public int getHuelleInProzent() 
	{
		return huelleInProzent;
	}

	public void setHuelleInProzent(int zustandhuelleInProzentNeu) 
	{
		this.huelleInProzent = zustandhuelleInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() 
	{
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int zustandlebenserhaltungssystemeInProzentNeu) 
	{
		this.lebenserhaltungssystemeInProzent = zustandlebenserhaltungssystemeInProzentNeu;
	}

	public int getAndroidenAnzahl() 
	{
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) 
	{
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() 
	{
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) 
	{
		this.schiffsname = schiffsname;
	}
	
	/**
	 * Diese Methode f�gt der Arraylist ladungsverzeichnis eine neu erstellte Ladung aus der Ladung.java als Parameter wird die Ladung �bergeben
	 * @see Ladung.java
	 * @param neueLadung �bergibt die angelegte Ladung aus der Klasse Ladung.java vom Typ Ladung
	 */
	//Ladung wird dem Raumschiff angeh�ngt
	public void addLadung(Ladung neueLadung)
	{
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Diese Methode schie�t ein Photonentorpedo auf das im Parameter r �bergebene Raumschiff.
	 * Es geschieht eine Abfrage, ob �berhaupt Photonentorpedos vorhanden sind.
	 * Sind keine Photonentorpedos im Raumschiff vorhanden, wird einfach nur "-=*Click*=-" ausgegeben.
	 * Sollten Photonentorpedos vorhanden sein, wird die Anzahl der vorhandenen Torpedos um 1 verringert 
	 * und es wird die Methode nachrichtAnAlle() ausgef�hrt und die Nachricht "Photonentorpedos abgeschossen!" �bergeben.
	 * Als letztes wird die Methode treffer(r) aufgerufen und das getroffene Raumschiff wird �bergeben.
	 * @param r �bergibt das Raumschiff, das abgeschossen werden soll vom Typ Raumschiff
	 */
	//Schie�t ein Photonentorpedo
	public void photonentorpedoSchiessen(Raumschiff r) 
	{
		
		if(photonentorpedoAnzahl <= 0)
		{
			nachrichtAnAlle("-=*Click*=-");
		}
		else
		{
			setPhotonentorpedoAnzahl(this.photonentorpedoAnzahl - 1);
			nachrichtAnAlle("Photonentorpedos abgeschossen!");
			treffer(r);
		}
		
	}
	
	/**
	 * Diese Methode schie�t die Phaserkanone auf das �bergebene Raumschiff r ab.
	 * Es erfolgt eine Abfrage, ob die Energieversorgung kleine 50 betr�gt. 
	 * Sollte dies der Fall sein, wird nur die Nachricht -=*Click*=- an die Methode nachrichtAnAlle() �bergeben.
	 * Sollte die Energieversorgung �ber oder gleich 50 betragen, wird die Nachricht Phaserkanone abgefeuert an die Methode nachrichtAnAlle() �bergeben
	 * und die Methode treffer() wird aufgerufen, sowie das getroffene Raumschiff als Parameter �bergeben.
	 * @param r �bergibt das Raumschiff, das abgeschossen werden soll vom Typ Raumschiff
	 */
	//Schie�t die Phaserkanone
	public void phaserkanoneSchiessen(Raumschiff r) 
	{
		
		if(getEnergieversorgungInProzent() < 50)
		{
			nachrichtAnAlle("-=*Click*=-");
		}
		else
		{
			nachrichtAnAlle("Phaserkanone abgefeuert");
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
			treffer(r);
		}
	}
	
	/**
	 * Diese Methode steuert den Ablauf, wenn ein Raumschiff von einem anderen Raumschiff getroffen wurde.
	 * Es wird die Methode nachrichtAnAlle() ausgef�hrt und �bergeben, dass das jeweilige Raumschiff getroffen wurde.
	 * Danach wir das Schild des getroffenen Raumschiffes um 50 verringert.
	 * Nun folgt eine Abfrage, ob die Schilde des getroffenen Raumschiffes unter oder gleich 0 sind.
	 * Sollte dies der Fall sein, wird der Wert der Huelle des getroffenen Raumschiffes um 50 verringert, sowie auch die Energieversorgung um 50 verringert.
	 * Danach erfolgt eine erneute Abfrage, ob der Wert der Huelle des getroffenen Raumschiffes nun unter oder gleich 0 liegt.
	 * Sollte dies der Falls sein, wird die Methode nachrichtAnAlle() aufgerufen und �bergeben, dass die Lebenserhaltungssysteme 
	 * des getroffenen Raumschiffes vollst�ndg zerst�rt wurden.
	 * @param r �bergibt das Raumschiff, das getroffen wurde vom Typ Raumschiff
	 */
	//Raumschiffe werden getroffen sowie Werte vermindert
	private void treffer(Raumschiff r) 
	{
		nachrichtAnAlle(r.getSchiffsname() + " wurde getroffen!");
		
		r.setSchildeInProzent(r.getSchildeInProzent() - 50);
		
		if(r.getSchildeInProzent() <= 0)
		{
			r.setHuelleInProzent(r.getHuelleInProzent() - 50);
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);
		}
		
		else if(r.getHuelleInProzent() <= 0)
		{
			System.out.println("Die Lebenserhaltungssysteme von: " + r.getSchiffsname() + " wurden zerst�rt!");
		}
	}
	
	/**
	 * Diese Methode printed eine Nachricht aus, die vorher als Parameter �bergeben worden ist.
	 * Danach wird die Nachricht in der ArrayList broadcastKommunikator angeh�ngt.
	 * @param message �bergibt die Nachricht die an Alle ausgegeben werden soll als String
	 */
	//Gibt eine Nachricht an alle aus
	public void nachrichtAnAlle(String message)
	{
		System.out.printf("\n" + message);
		this.broadcastKommunikator.add(message);
	}
	
	/**
	 * Diese Methode gibt alle Eintr�ge die gespeichert wurden aus dem broadcastKommunikator aus
	 * @return Alle im broadcastKommunikator gespeicherten Nachrichten von jedem Aufruf der Methode nachrichtAnAlle()
	 */
	//Broadcastkommunikator wird ausgegeben
	public ArrayList<String> eintraegeLogbuchZurueckgeben()
	{
		return broadcastKommunikator;
	}
	
	/**
	 * Diese Methode l�dt Photonentorpedos aus der vorhandenen Ladung in das Raumschiff.
	 * Es wird eine for-Schleife gestartet, die durch das Ladungsverzeichnis geht.
	 * In dieser Schleife wird �berpr�ft, ob das Ladungsverzeichnis �berhaupt eine Ladung mit dem Namen Photonentorpedos besitzt.
	 * Sollte das Ladungsverzeichnis so eine Ladung besitzen, wird eine neue Abfrage verwendet, bei der geguckt wird, ob die Anzahl
	 * der Torpedos die eingesetzt werden sollen h�her sind als die vorhandenen in der Ladung. Ist das der Fall, wird die gesamte Anzahl der
	 * Torpedos aus der Ladung in das Raumschiff eingesetzt und es wird �ber Sysout ausgegeben, wie viele Torpedos eingesetzt wurden. 
	 * Andernfalls wird nur die angegebene Anzahl der Torpedos aus der Ladung in das Raumschiff eingesetzt und die Anzahl der vorhandenen 
	 * Torpedos in der Ladung um die �bergebene Anzahl verringert und �ber Sysout ausgegeben, wie viele Torpedos eingesetzt wurden. 
	 * Sollten keine Torpedos in der Ladung vorhanden sein, wird per Sysout einfach eine Nachricht ausgegeben, dass
	 * keine vorhanden sind und es wird durch die Methode nachrichtAnAlle() eine Nachricht gesendet.
	 * @param anzahlTorpedos die Anzahl der Torpedos die eingef�gt werden soll
	 */
	//Photonentorpedos aus der Ladung werden eingef�gt
	public void photonentorpedosLaden(int anzahlTorpedos) 
	{
		int zaehler = 0;
		
		for(int i = 0; i < ladungsverzeichnis.size(); i++)
		{		
			if(ladungsverzeichnis.get(i).getBezeichnung().contains("Photonentorpedos") == true)
			{
				if(anzahlTorpedos > ladungsverzeichnis.get(i).getMenge())
				{					
					setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + ladungsverzeichnis.get(i).getMenge());
					ladungsverzeichnis.get(i).setMenge(0);
					System.out.println("\n" + ladungsverzeichnis.get(i).getMenge() + " Photonentorpedo(s) eingesetzt");
					zaehler++;
				}
				
				else
				{
					ladungsverzeichnis.get(i).setMenge(ladungsverzeichnis.get(i).getMenge() - anzahlTorpedos);
					setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + anzahlTorpedos);
					System.out.println(anzahlTorpedos + " Photonentorpedo(s) eingesetzt");
					zaehler++;
				}
			}
		}
		
		if(zaehler == 0)
		{
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
			zaehler = 0;
		}
		
		
	}
	
	/**
	 * Diese Methode berechnet den Reperaturwert f�r die Schiffsstrukturen und f�hrt diese Reperatur dann aus.
	 * 
	 * @param schutzschilde
	 * @param energieversorgung
	 * @param schiffshuelle
	 * @param anzahlDroiden
	 */
	//Raumschiffe Reperatur
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) 
	{
		int truezaehler = 0;
		int reperaturwert;
		Random zufall = new Random();
		int random = zufall.nextInt(100 + 1) + 1;
		
		if(anzahlDroiden > getAndroidenAnzahl())
		{
			anzahlDroiden = getAndroidenAnzahl();
		}
		
		if(schutzschilde)
		{
			truezaehler++;
		}
		
		if(energieversorgung)
		{
			truezaehler++;
		}
		
		if(schiffshuelle)
		{
			truezaehler++;
		}
		
		reperaturwert = (random * anzahlDroiden)/truezaehler;
		
		if(schutzschilde)
		{
			setSchildeInProzent(getSchildeInProzent() + reperaturwert);
			
			if(getSchildeInProzent() > 100)
			{
				setSchildeInProzent(100);
			}
		}
		
		if(energieversorgung)
		{
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + reperaturwert);
			
			if(getEnergieversorgungInProzent() > 100)
			{
				setEnergieversorgungInProzent(100);
			}
		}
		
		if(schiffshuelle)
		{
			setHuelleInProzent(getHuelleInProzent() + reperaturwert);
			
			if(getHuelleInProzent() > 100)
			{
				setHuelleInProzent(100);
			}
		}
		
		
	}
	
	/**
	 * Diese Methode gibt alle Zust�nde eines Raumschiffes in der Console wieder.
	 * Die Werte werden alle einfach als Sysout ausgegeben.
	 */
	//Gibt den Zustand des jeweiligen Raumschiffs aus
	public void zustandRaumschiff() 
	{		
		System.out.printf("\n\nZustand von = %s\n", getSchiffsname());
		System.out.println("Photonentorpedos = " + photonentorpedoAnzahl);
		System.out.println("Energieversorgung = " + getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde = " + getSchildeInProzent() + "%");
		System.out.println("Huelle = " + getHuelleInProzent() + "%");
		System.out.println("Lebenserhaltungssysteme = " + getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Androidenanzahl = " + androidenAnzahl);
		
	}
	
	/**
	 * Diese Methode gibt das Ladungsverzeichnis eines Raumschiffes aus.
	 * Dazu wird zuerst eine Abfrage gestartet, ob sich �berhaupt Ladung im Ladungsverzeichnis des Raumschiffes befindet.
	 * Sollte sich keine Ladung in dem Raumschiff befinden, wird als Sysout ausgegeben, dass nichts im Ladungsverzeichnis vorhanden ist.
	 * Sollte Ladung im Ladungsverzeichnis des Raumschiffes vorhanden sein, wird zuerst als Sysout der Name des Raumschiffes ausgegeben.
	 * Daraufhin wird in einer for-Schleife jede Bezeichnung und die dazugeh�rige Menge aus dem Ladungsvereichnis ausgegeben.
	 */
	//Ausgabe des Ladungsverzeichnisses
	public void ladungsverzeichnisAusgeben() 
	{
		if(this.ladungsverzeichnis.size() <= 0)
		{
			System.out.println("Es ist nichts im Ladungsverzeichnis vorhanden!");
		}
		else {
			System.out.println("Ladungsverzeichnis von " + getSchiffsname() + ":");
			for(int i = 0; i < ladungsverzeichnis.size(); i++) {
				System.out.println(ladungsverzeichnis.get(i).getBezeichnung()+": " + ladungsverzeichnis.get(i).getMenge());
			}
			System.out.println("\n");
		}
	}
	
	/*
	 * Diese Methode �berpr�ft durch eine for-Schleife, welche Position in der Ladung bei der Menge den Wert 0 oder niedriger enth�lt.
	 * Sollte eine Position 0 oder niedriger enthalten, so wird es aus der ArrayList ladungsverzeichnis entfernt mit dem Befehl .remove()
	 */
	//Entfernt Ladungseintr�ge deren Menge = 0 ist
	public void ladungsverzeichnisAufraeumen() 
	{
		
		for(int i = 0; i < ladungsverzeichnis.size(); i++)
		{
			if(ladungsverzeichnis.get(i).getMenge() <= 0)
			{
				ladungsverzeichnis.remove(i); 
			}
		}
	}
	
	
}
