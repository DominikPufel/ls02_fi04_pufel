package kdr;

import java.util.Random;

public class Krieg_der_Raumschiffe {

	public static void main(String[] args) {
		
		//Raumschiffe initialisieren
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		
		//Ladung initialisieren
		Ladung ferengischneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung borgschrott = new Ladung("Borg-Schrott", 5);
		Ladung rotematerie = new Ladung("Rote Materie", 2);
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung klingonenschwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung plasmawaffe = new Ladung("Plasma-Waffe", 50);
		Ladung photonentorpedo = new Ladung("Photonentorpedos", 3);
		
		
		//Ladung zuweisen
		klingonen.addLadung(ferengischneckensaft);
		klingonen.addLadung(klingonenschwert);
		
		romulaner.addLadung(borgschrott);
		romulaner.addLadung(rotematerie);
		romulaner.addLadung(plasmawaffe);
		
		vulkanier.addLadung(forschungssonde);
		vulkanier.addLadung(photonentorpedo);
		
		
		//Spielablauf
		
		klingonen.photonentorpedoSchiessen(romulaner);
		
		romulaner.phaserkanoneSchiessen(klingonen);
		
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		
		vulkanier.reperaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
	}

}
