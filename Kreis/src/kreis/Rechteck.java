package kreis;

import com.sun.javafx.scene.paint.GradientUtils.Point;

public class Rechteck {
	
	private double a, b;
	private Point mittelpunkt;
    
	public Rechteck(double a, double b)
	{
	  setSeitea(a);
	  setSeiteb(b);
	}
	
	
	public double getSeitea() {
		return a;
	}
	
	public void setSeitea(double a) {
		this.a = a; 
	}
	
	public double getSeiteb() {
		return b;
	}
	
	public void setSeiteb(double b) {
		this.b = b;
	}

	
	public double getFlaeche() {
		return a * b;
	}
	
	public double getUmfang() {
		return (2 * a) + (2 * b);
	}
	
	public double getDiagonale()
	{
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}
	
	public Point getMittelpunkt()
	{
		return mittelpunkt;
		
	}
	

}
